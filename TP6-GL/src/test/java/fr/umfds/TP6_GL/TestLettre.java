package fr.umfds.TP6_GL;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestLettre {

	private static float tolerancePrix=0.001f;
	
	static Lettre lettre1;
	static Lettre lettre2;
	
	@BeforeAll
	static void initAll() {
		lettre1 = new Lettre("Le pere Noel", "famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel", "famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
	}
	
	@Test
	public void testToString1() {
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire", lettre1.toString());
	}
	
	@Test
	public void testToString2() {
		assertEquals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence", lettre2.toString());
	}
	
	@Test
	public void testAffranchissement1() {
		assertTrue(Math.abs(lettre1.tarifAffranchissement()-1.0f)<tolerancePrix);
	}
	
	@Test
	public void testAffranchissement2() {
		assertTrue(Math.abs(lettre2.tarifAffranchissement()-2.3f)<tolerancePrix);
	}
	
	@Test
	public void testTarifRemboursement1() {
		assertTrue(Math.abs(lettre1.tarifRemboursement()-1.5f)<tolerancePrix);
	}
	
	@Test
	public void testTarifRemboursement2() {
		assertTrue(Math.abs(lettre2.tarifRemboursement()-15.0f)<tolerancePrix);
	}

}
