package fr.umfds.TP6_GL;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestColis {

	private static float tolerancePrix=0.001f;
	
	static Colis colis1;
	
	@BeforeAll
	static void initAll() {
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest", 
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	}
	
	@Test
	public void testToString() {
		assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0", colis1.toString());
	}
	
	//d'après la prof, on doit pas utiliser un assertTrue mais plutôt un assertEquals(3.5, colis1.tarifAffranchissement(), tolerancePrix)
	@Test
	public void testAffranchissement() {
		assertTrue(Math.abs(colis1.tarifAffranchissement()-3.5f)<tolerancePrix);
	}
	
	@Test
	public void testTarifRemboursement() {
		assertTrue(Math.abs(colis1.tarifRemboursement()-100.0f)<tolerancePrix);
	}

}
