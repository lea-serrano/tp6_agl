package fr.umfds.TP6_GL;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestSacPostal {

	private static float tolerancePrix = 0.001f;
	private static float toleranceVolume = 0.0000001f;

	static Lettre lettre1;
	static Lettre lettre2;
	static Colis colis1;
	static SacPostal sac1;
	static SacPostal sac2;

	@BeforeAll
	static void initAll() {
		lettre1 = new Lettre("Le pere Noel", "famille Kirik, igloo 5, banquise nord", "7877", 25, 0.00018f,
				Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel", "famille Kouk, igloo 2, banquise nord", "5854", 18, 0.00018f,
				Recommandation.deux, true);
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 1024, 0.02f,
				Recommandation.deux, "train electrique", 200);

		sac1 = new SacPostal();
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);

		sac2 = sac1.extraireV1("7877");
	}

	@Test
	public void testValeurRemboursement() {
		assertFalse(Math.abs(sac1.valeurRemboursement() - 116.5f) < tolerancePrix);
	}

	@Test
	public void testToleranceVolume() {
		assertFalse(Math.abs(sac1.getVolume() - 0.025359999558422715f) < toleranceVolume);
	}

	@Test
	public void testExtraction() {
		assertTrue(sac2.getVolume() - 0.02517999955569394f < toleranceVolume);
	}

}
