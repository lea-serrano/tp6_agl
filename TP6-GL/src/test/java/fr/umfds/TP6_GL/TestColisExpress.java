package fr.umfds.TP6_GL;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestColisExpress {
	
	static ColisExpress colis1;
	
	@BeforeAll
	static void initAll() throws ColisExpressInvalide {
		colis1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", 
				"7877", 10, 0.02f, Recommandation.deux, "train electrique", 200, true);
	}
	
	@Test
	public void testToString() {
		assertEquals("Colis express 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0/10.0/0", colis1.toString());
	}
	
	@Test
	public void testAffranchissement() {
		assertEquals(33f, colis1.tarifAffranchissement());
	}

}
