package fr.umfds.TP6_GL;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestCourriel {

	static Courriel c1;
	static Courriel c2;
	static Courriel c3;
	static Courriel c4;
	static Courriel c5;
	
	@BeforeAll
	static void initAll() {
		c1 = new Courriel("toto@titi.fr", "Salut!", "Bonjour, comment ça va ? Voici une photo de chat en PJ!", "lienpiecejointe");
		c2 = new Courriel("tototiti.fr", "Salut!", "Bonjour, comment ça va ? Voici une photo de chat en PJ!", "lienpiecejointe");
		c3 = new Courriel("toto@titi.fr", null, "Bonjour, comment ça va ? Voici une photo de chat en PJ!", "lienpiecejointe");
		c4 = new Courriel("toto@titi.fr", "Salut!", "Bonjour, comment ça va ? Voici une photo de chat en PJ!", null);
		c5 = new Courriel("toto@titi.fr", "Salut!", "Bonjour, comment ça va ? Voici une photo!", null);
	}
	
	@Test
	public void testEnvoyerValide() {
		assertTrue(c1.envoyer());
	}
	
	@Test
	public void testEnvoyerMailNonValide() {
		assertFalse(c2.envoyer());
	}
	
	@Test
	public void testEnvoyerTitreNonValide() {
		assertFalse(c3.envoyer());
	}
	
	@Test
	public void testEnvoyerPJNonValide() {
		assertFalse(c4.envoyer());
	}
	
	@Test
	public void testEnvoyerSansPJValide() {
		assertTrue(c5.envoyer());
	}

}
