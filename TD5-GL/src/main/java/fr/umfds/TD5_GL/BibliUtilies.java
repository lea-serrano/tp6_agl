package fr.umfds.TD5_GL;

import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BibliUtilies {
	
	IGlobalBibliographyAccess gbAccess;
	private static int delaiInventaire = 12;
	private Clock clock;
	
	public BibliUtilies() {
		super();
		gbAccess = new GlobalBibliographyAccess();
		clock = Clock.systemDefaultZone();
	}
	
	public List<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref) {
		ArrayList<NoticeBibliographique> noticesMemeAuteur = new ArrayList<NoticeBibliographique>();
		ArrayList<NoticeBibliographique> lecturesConnexes = new ArrayList<NoticeBibliographique>();
		
		boolean connexe = false;
		int compteur = lecturesConnexes.size();
		
		noticesMemeAuteur = gbAccess.noticesDuMemeAuteurQue(ref);
		
		//MA VERSION
		for (NoticeBibliographique elt : noticesMemeAuteur) {
			for (int i = 0; i < compteur; i++) {
				if (elt.getAuteur() == lecturesConnexes.get(i).getAuteur() && compteur < 5 && elt.getTitre() != noticesMemeAuteur.get(i).getTitre()) {
					connexe = true;
				}
			}
			if (connexe) {
				lecturesConnexes.add(elt);
			}
		}
		
		
		//CORRECTION 
		
		/*Set<String> nameSet = new HashSet<>();
		return res.Stream()
					.filter(n -> !n.getTitre().equals(ref.getTitre)))
					.filter(e ->  nameSet.add(e.getTitre()))
					.limit(5)
					.collect(Collectors.toList());*/
		
		
		return lecturesConnexes;
	}
	
	public boolean prevoirInventaire() {
		return Math.abs(LocalDate.now(clock).until(Bibliothèque.getInstance().getLastInventaire()).toTotalMonths())>=delaiInventaire;
	}
	
	public NoticeStatus ajoutNotice(String isbn) throws AjoutImpossibleException {
		NoticeStatus res = null;
		try {
			NoticeBibliographique nb = gbAccess.getNoticeFromIsbn(isbn);
			res = Bibliothèque.getInstance().addNotice(nb);
		} catch (IncorrectIsbnException e) {
			throw new AjoutImpossibleException();
		}
		return res;
	}

}
